# AdventureJam2020: A Strange Alchemy Adventure

## __Team__
- Corona&Co.

## __Members__
__Game Designers__
- [Anthony Santangelo]()
- [Chiara Buoncristiani]()

__Game Programmers__
- [Michele Santoni, GitLab](https://gitlab.com/MicheleSantoni) | [Michele Santoni, GitHub](https://github.com/CthulhusMadness)
- [Francesco Catania](https://github.com/FrancescoCataniaDev/FrancescoCataniaDev.github.io)

## __GameJoltPage__
- [A Strange Alchemy Adventure](https://gamejolt.com/games/a_strange_alchemy_adventure/493453)