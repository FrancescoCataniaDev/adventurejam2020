﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public Animator anim;
    public GameObject handle;
    public float moveSpeed = 0.1f;
    private Rigidbody rb;
    private Vector3 mousePosition;
    private Vector3 position = new Vector3();
    private Vector3 movement;
    private float table = 0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetLayerWeight(1, 1f);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            anim.SetLayerWeight(2, 1f);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            anim.SetLayerWeight(3, 1f);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            anim.SetLayerWeight(4, 1f);
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            anim.SetLayerWeight(5, 1f);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            anim.SetLayerWeight(1, 0f);
        }

        if (Input.GetKeyUp(KeyCode.R))
        {
            anim.SetLayerWeight(2, 0f);
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            anim.SetLayerWeight(3, 0f);
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            anim.SetLayerWeight(4, 0f);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            anim.SetLayerWeight(5, 0f);
        }
    }

    private void Movement()
    {
        if (Input.GetMouseButton(1))
        {
            Vector3 newRotation = new Vector3(0, Input.GetAxis("Mouse X"), 0)* Time.deltaTime * moveSpeed;
            handle.transform.Rotate(newRotation);
        }
        else 
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Vector3 camPos = ray.origin;
            Vector3 mouseDir = ray.direction;

            float t = (table - camPos.y) / mouseDir.y;
            Vector3 interPos = camPos + t * mouseDir;
            float posX = transform.position.x;
            float posZ = transform.position.z;
            posX = Mathf.Clamp(interPos.x, -3, 3);
            posZ = Mathf.Clamp(interPos.z, -3, 3);
            transform.position = new Vector3(posX, interPos.y, posZ);
        }
    }

    private void FixedUpdate()
    {
        Movement();
        // rb.MovePosition(position + Vector3.up);
    }
}